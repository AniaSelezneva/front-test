(function ($) {
    "use strict";

    /**
     * Табы
     */
    $.fn.tabs = function () {
        var $self = $(this);
        var $tabHeaders = $self
            .find(".js-tab-header")
            .filter(function (index, el) {
                return $(el).parentsUntil($self).length === 1;
            });
        var $tabContent = $self
            .find(".js-tab-content")
            .filter(function (index, el) {
                return $(el).parentsUntil($self).length === 1;
            });

        /**
         * Активация таба по его индексу
         * @param {Number} index - индекс таба, который нужно активировать
         */
        var selectTab = function (index) {
            $tabHeaders.removeClass("active").eq(index).addClass("active");
            $tabContent.removeClass("active").eq(index).addClass("active");
        };

        /**
         * Инициализаиця
         */
        var init = function () {
            selectTab(0);

            // Обработка событий
            $tabHeaders.on("click", function () {
                selectTab($(this).index());
            });
        };

        init();

        this.selectTab = selectTab;

        return this;
    };

    // Инициализируем табы на всех блоках с классом 'js-tabs'
    $(".js-tabs").each(function () {
        $(this).data("tabs", $(this).tabs());
    });
})(jQuery);

/*
 Перетаскивание изображения нажатием мышки и с помощью тачскрина
*/
{
    let dragging_on = false; // нажата ли клавиша мышки
    const image_container = document.querySelector(".drag");
    let image = image_container.firstElementChild;
    let img_top = parseInt(image.getBoundingClientRect().top, 10);
    let img_left = parseInt(image.getBoundingClientRect().left, 10);
    const last_mouse_pos = { x: 0, y: 0 };

    const parent_pos_in_document = {};
    const viewportOffset = image_container.getBoundingClientRect();
    parent_pos_in_document.top = viewportOffset.top;
    parent_pos_in_document.left = viewportOffset.left;

    const current_mouse_position = {};

    image_container.addEventListener("mousedown", (e) => {
        // кнопка мышки нажата - перетаскивание началось
        dragging_on = true;

        last_mouse_pos.x = e.pageX - parent_pos_in_document.left;
        last_mouse_pos.y = e.pageY - parent_pos_in_document.top;
    });

    document.addEventListener("mouseup", () => {
        dragging_on = false; // остановить перетаскивание
        image_container.style.cursor = "grab";
    });

    image_container.addEventListener("mousemove", (e) => {
        if (dragging_on) {
            image_container.style.cursor = "grabbing";
            current_mouse_position.x = e.pageX - parent_pos_in_document.left;
            current_mouse_position.y = e.pageY - parent_pos_in_document.top;

            const change_x = current_mouse_position.x - last_mouse_pos.x;
            const change_y = current_mouse_position.y - last_mouse_pos.y;

            let img_top_new = img_top + change_y;
            let img_left_new = img_left + change_x;

            // не выходить за границы изображения
            if (img_top_new > 0) img_top_new = 0;
            if (img_top_new < image_container.offsetHeight - image.offsetHeight)
                img_top_new = image_container.offsetHeight - image.offsetHeight;

            if (img_left_new > 0) img_left_new = 0;
            if (img_left_new < image_container.offsetWidth - image.offsetWidth)
                img_left_new = image_container.offsetWidth - image.offsetWidth;

            // изменить положение изображения
            image.style.top = `${img_top_new}px`;
            image.style.left = `${img_left_new}px`;

            img_top = img_top_new;
            img_left = img_left_new;

            last_mouse_pos.x = current_mouse_position.x;
            last_mouse_pos.y = current_mouse_position.y;
        }
    });

    // поддержка тачскрина
    image_container.addEventListener("touchstart", (e) => {
        e = e.changedTouches[0];
        last_mouse_pos.x = e.pageX - parent_pos_in_document.left;
        last_mouse_pos.y = e.pageY - parent_pos_in_document.top;
    });

    image_container.addEventListener("touchend", (e) => {
        dragging_on = false;
    });

    image_container.addEventListener("touchmove", (e) => {
        dragging_on = true;
        e = e.changedTouches[0];
        if (dragging_on) {
            current_mouse_position.x = e.pageX - parent_pos_in_document.left;
            current_mouse_position.y = e.pageY - parent_pos_in_document.top;

            const change_x = current_mouse_position.x - last_mouse_pos.x;
            const change_y = current_mouse_position.y - last_mouse_pos.y;

            let img_top_new = img_top + change_y;
            let img_left_new = img_left + change_x;

            // не выходить за границы изображения
            if (img_top_new > 0) img_top_new = 0;
            if (img_top_new < image_container.offsetHeight - image.offsetHeight)
                img_top_new = image_container.offsetHeight - image.offsetHeight;

            if (img_left_new > 0) img_left_new = 0;
            if (img_left_new < image_container.offsetWidth - image.offsetWidth)
                img_left_new = image_container.offsetWidth - image.offsetWidth;

            // изменить положение изображения
            image.style.top = `${img_top_new}px`;
            image.style.left = `${img_left_new}px`;

            img_top = img_top_new;
            img_left = img_left_new;

            last_mouse_pos.x = current_mouse_position.x;
            last_mouse_pos.y = current_mouse_position.y;
        }
    });
}

/*
 Перемещения изображения по наведению мышкой и при помощи тачскрина
*/
{
    const image_container = document.querySelector(".hover");
    let image = image_container.firstElementChild;
    let img_top = parseInt(image.getBoundingClientRect().top, 10);
    let img_left = parseInt(image.getBoundingClientRect().left, 10);
    const last_mouse_pos = { x: 0, y: 0 };

    // нужно будет чтобы дотягивать изображения до края
    // (мышка в правом верхнем углу - правый верхний угол изображения
    // соответствует правому верхнему углу контейнера)
    let unitX, unitY;

    const parent_pos_in_document = {};
    const viewportOffset = image_container.getBoundingClientRect();
    parent_pos_in_document.top = viewportOffset.top;
    parent_pos_in_document.left = viewportOffset.left;

    const current_mouse_position = {};

    image_container.addEventListener("mousemove", (e) => {
        unitX = image.offsetWidth / image_container.offsetWidth;
        unitY = image.offsetHeight / image_container.offsetHeight;

        current_mouse_position.x = e.pageX - parent_pos_in_document.left;
        current_mouse_position.y = e.pageY - parent_pos_in_document.top;

        const change_x = current_mouse_position.x - last_mouse_pos.x;
        const change_y = current_mouse_position.y - last_mouse_pos.y;

        let img_top_new = img_top - change_y * unitY;
        let img_left_new = img_left - change_x * unitX;

        // не выходить за границы изображения
        if (img_top_new > 0) img_top_new = 0;
        if (img_top_new < image_container.offsetHeight - image.offsetHeight)
            img_top_new = image_container.offsetHeight - image.offsetHeight;
        if (img_left_new > 0) img_left_new = 0;
        if (img_left_new < image_container.offsetWidth - image.offsetWidth)
            img_left_new = image_container.offsetWidth - image.offsetWidth;

        // изменить положение изображения
        image.style.top = `${img_top_new}px`;
        image.style.left = `${img_left_new}px`;

        img_top = img_top_new;
        img_left = img_left_new;

        last_mouse_pos.x = current_mouse_position.x;
        last_mouse_pos.y = current_mouse_position.y;
    });

    // поддержка тачскрина
    image_container.addEventListener("touchmove", (e) => {
        e = e.changedTouches[0];

        unitX = image.offsetWidth / image_container.offsetWidth;
        unitY = image.offsetHeight / image_container.offsetHeight;

        current_mouse_position.x = e.pageX - parent_pos_in_document.left;
        current_mouse_position.y = e.pageY - parent_pos_in_document.top;

        const change_x = current_mouse_position.x - last_mouse_pos.x;
        const change_y = current_mouse_position.y - last_mouse_pos.y;

        let img_top_new = img_top - change_y * unitY;
        let img_left_new = img_left - change_x * unitX;

        // не выходить за границы изображения
        if (img_top_new > 0) img_top_new = 0;
        if (img_top_new < image_container.offsetHeight - image.offsetHeight)
            img_top_new = image_container.offsetHeight - image.offsetHeight;

        if (img_left_new > 0) img_left_new = 0;
        if (img_left_new < image_container.offsetWidth - image.offsetWidth)
            img_left_new = image_container.offsetWidth - image.offsetWidth;

        // изменить положение изображения
        image.style.top = `${img_top_new}px`;
        image.style.left = `${img_left_new}px`;

        img_top = img_top_new;
        img_left = img_left_new;

        last_mouse_pos.x = current_mouse_position.x;
        last_mouse_pos.y = current_mouse_position.y;
    });
}

/*
 Уменьшение / увеличение шапки
*/
{
    const header = document.querySelector(".header");
    document.addEventListener("scroll", function () {
        // плавно уменьшить
        if (window.scrollY > 0) {
            header.style.maxHeight = "70px";
            header.style.transition = "max-height 0.50s ease-in";
        }
        // плавно увеличить
        else {
            header.style.maxHeight = "120px";
            header.style.transition = "max-height 0.50s ease-in";
        }
    });
}
