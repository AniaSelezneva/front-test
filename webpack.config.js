const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");
const globImporter = require("node-sass-glob-importer");

module.exports = {
    mode: "production",

    // 2 entry points
    entry: {
        main: { import: "./src/js/tabs.js", filename: "./js/scripts.min.js" },
        styles: {
            import: "./src/scss/styles.scss",
        },
    },
    output: {
        assetModuleFilename: "img/[name].[ext]",
    },

    plugins: [
        // exclude node modules
        new webpack.SourceMapDevToolPlugin({
            filename: "[file].map",
            exclude: /vendor\.min\.js/,
        }),

        new MiniCssExtractPlugin({
            filename: "css/[name].min.css",
        }),
        // make $ available globally
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
        }),
        // get main html file
        new HtmlWebpackPlugin({
            inject: false,
            template: path.resolve(__dirname, "./src/index.html"),
        }),
    ],
    optimization: {
        // exclude license file
        minimizer: [
            new TerserPlugin({
                extractComments: false,
                terserOptions: {
                    format: {
                        comments: false,
                    },
                },
            }),
        ],
        splitChunks: {
            cacheGroups: {
                // modules
                common: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "js/vendor.min",
                    chunks: "all",
                },
            },
        },
    },

    module: {
        rules: [
            {
                test: /\.(jpg|png|gif|svg)$/,
                type: "asset/resource",
            },
            {
                test: /\.(s*)css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader",
                        options: { sourceMap: true },
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                            sassOptions: {
                                importer: globImporter(),
                            },
                        },
                    },
                ],
            },
        ],
    },
};
